FROM node

RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN yarn install

copy . /app

#run yarn test
run yarn build

EXPOSE 3000

CMD yarn start
